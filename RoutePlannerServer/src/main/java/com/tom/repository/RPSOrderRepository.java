package com.tom.repository;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import com.tom.dto.RpsOrderList;
import com.tom.dto.RpsOrders;

@Component
@Scope("singleton")
public class RPSOrderRepository {
		
	private RpsOrderList rpsOrderList;

	public RpsOrderList getRpsOrderList() {
		return rpsOrderList;
	}

	public void setRpsOrderList(RpsOrderList rpsOrderList) {
		
		if(this.rpsOrderList==null){
		this.rpsOrderList = rpsOrderList;
		}else{
			List<RpsOrders> repo=Arrays.asList(this.rpsOrderList.getRpsOrders());
			repo.addAll(Arrays.asList(rpsOrderList.getRpsOrders()));
			this.rpsOrderList.setRpsOrders((RpsOrders[]) repo.toArray());
		}
	}
	

}
