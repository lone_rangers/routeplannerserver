package com.tom.dao;

import java.util.ArrayList;
import java.util.List;

import org.bson.Document;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.google.gson.Gson;
import com.mongodb.Block;
import com.mongodb.MongoClient;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoDatabase;
import com.tom.dto.RPSGenericResponse;
import com.tom.dto.RpsOrderList;
import com.tom.dto.RpsOrders;
import com.tom.repository.RPSOrderRepository;

@Repository
public class RPSOrderDaoNoSQLImpl implements RPSOrderDao{

	@Autowired
	MongoClient mongoClient;

	private RPSGenericResponse rpsGenericResponse=new RPSGenericResponse();

	//save orders
	public RPSGenericResponse storeorders(RpsOrderList rpsOrderList) {

		//get list of order
		RpsOrders[] rpsOrders=rpsOrderList.getRpsOrders();

		//get the db
		MongoDatabase db = mongoClient.getDatabase("rps_database");
		//todo:improve the logic
		for(RpsOrders rpsOrder:rpsOrders){
			Gson gson= new Gson();
			String orderJson=gson.toJson(rpsOrder);
			System.out.println("The serialised Java object is:"+orderJson);   
			db.getCollection("rps_orders").insertOne(new Document("order",orderJson));
		}
		rpsGenericResponse.setRPSGenericResponseMessage("Orders added to Mongo DB");
		return rpsGenericResponse;
	}

	public RpsOrderList getOrders(String someParameter) {

		//to do: improve the logic
		MongoDatabase db = mongoClient.getDatabase("rps_database");
		FindIterable<Document> iterable = db.getCollection("rps_orders").find();

		final List<RpsOrders> orderList=new ArrayList<RpsOrders>();

		iterable.forEach(new Block<Document>() {
			@Override
			public void apply(final Document document) {
				System.out.println(document.get("order"));
				Gson gson = new Gson();
				System.out.println("Converting the document to json"+gson.fromJson(document.get("order").toString(), RpsOrders.class));
				RpsOrders order=gson.fromJson(document.get("order").toString(), RpsOrders.class);
				orderList.add(order);
			}
		});

		RpsOrders[] rpsOrders=orderList.toArray(new RpsOrders[0]);
		RpsOrderList orList=new RpsOrderList();
		orList.setRpsOrders(rpsOrders);

		return orList;
	}
}
