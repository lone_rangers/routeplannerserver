package com.tom.dao;


import com.tom.dto.RPSGenericResponse;
import com.tom.dto.RpsOrderList;

public interface RPSOrderDao {
	
	RPSGenericResponse storeorders(RpsOrderList rpsOrderList);
	
	RpsOrderList getOrders(String someParameter);

}
