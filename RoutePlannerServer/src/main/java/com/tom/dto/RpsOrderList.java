package com.tom.dto;

import java.io.Serializable;

public class RpsOrderList
{
    private RpsOrders[] rpsOrders;

    public RpsOrders[] getRpsOrders ()
    {
        return rpsOrders;
    }

    public void setRpsOrders (RpsOrders[] rpsOrders)
    {
        this.rpsOrders = rpsOrders;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [rpsOrders = "+rpsOrders+"]";
    }
}
