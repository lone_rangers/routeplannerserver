package com.tom.dto;

public class RPSGenericResponse
{
    private String RPSGenericResponseMessage;

    public String getRPSGenericResponseMessage ()
    {
        return RPSGenericResponseMessage;
    }

    public void setRPSGenericResponseMessage (String RPSGenericResponseMessage)
    {
        this.RPSGenericResponseMessage = RPSGenericResponseMessage;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [RPSGenericResponseMessage = "+RPSGenericResponseMessage+"]";
    }
}
