package com.tom.dto;

public class RpsOrders
{
    private String orderNo;

    private String shipFrom;

    private String shipTo;
    
    private String latitude;
    
    private String longitude;

    public String getOrderNo ()
    {
        return orderNo;
    }

    public void setOrderNo (String orderNo)
    {
        this.orderNo = orderNo;
    }

    public String getShipFrom ()
    {
        return shipFrom;
    }

    public void setShipFrom (String shipFrom)
    {
        this.shipFrom = shipFrom;
    }

    public String getShipTo ()
    {
        return shipTo;
    }

    public void setShipTo (String shipTo)
    {
        this.shipTo = shipTo;
    }
    

    public String getLatitude() {
		return latitude;
	}

	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}

	public String getLongitude() {
		return longitude;
	}

	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}

	@Override
    public String toString()
    {
        return "ClassPojo [orderNo = "+orderNo+", shipFrom = "+shipFrom+", shipTo = "+shipTo+" latitude="+latitude+" longitude:"+longitude+" ]";
    }
}
