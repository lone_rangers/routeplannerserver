package com.tom.dto;

public class RPSAllMobileLocation {
	
	private RPSMobileLocation[] allMobileLocations;

	public RPSMobileLocation[] getAllMobileLocations() {
		return allMobileLocations;
	}

	public void setAllMobileLocations(RPSMobileLocation[] allMobileLocations) {
		this.allMobileLocations = allMobileLocations;
	}
	
	

}
