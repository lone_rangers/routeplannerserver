package com.tom.engine;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServlet;

import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.tom.dto.RPSAllMobileLocation;
import com.tom.dto.RPSMobileLocation;

@EnableAutoConfiguration
@RestController
@RequestMapping(value="/mobile")
public class MobileRestController extends HttpServlet {

	
	
	private Map<String,RPSMobileLocation> mobileLocationMap=new HashMap<>();

	//receive the current mobile coordinates
	@RequestMapping(value="/sendLocation",method=RequestMethod.POST)
	public void seedLocation(@RequestBody RPSMobileLocation rpsML){
		
		//see of already the mobile is there is there
		if(mobileLocationMap.containsKey(rpsML.getUnqueMobileIdentifier())){
			RPSMobileLocation mobileLocation=mobileLocationMap.get(rpsML.getUnqueMobileIdentifier());
			mobileLocation.setLatitude(rpsML.getLatitude());
			mobileLocation.setLongitude(rpsML.getLongitude());
			
		}else{
			RPSMobileLocation mobileLocation=new RPSMobileLocation();		
			mobileLocation.setUnqueMobileIdentifier(rpsML.getUnqueMobileIdentifier());
			mobileLocation.setLatitude(rpsML.getLatitude());
			mobileLocation.setLongitude(rpsML.getLongitude());
			mobileLocationMap.put(mobileLocation.getUnqueMobileIdentifier(), mobileLocation);			
		}
	}

//	//receive the current mobile coordinates
//	@RequestMapping(value="/getLocation",method=RequestMethod.GET)
//	public RPSMobileLocation getLocation(){
//
//		return mobileLocation;
//	}
	
	//receive the current mobile coordinates
	@RequestMapping(value="/getAllMobileLocations",method=RequestMethod.GET)
	public RPSAllMobileLocation getAllMobileLocations(){
		
		List<RPSMobileLocation> locationList=new ArrayList<>();
		
		for(Map.Entry<String,RPSMobileLocation> entry:mobileLocationMap.entrySet()){
			RPSMobileLocation mobLoc =entry.getValue();
			locationList.add(mobLoc);
		}
		
		RPSMobileLocation[] locArray=new RPSMobileLocation[locationList.size()];
		
		for(int i=0;i<locationList.size();i++){
			locArray[i]=locationList.get(i);
		}
		
		RPSAllMobileLocation allMob=new RPSAllMobileLocation();
		allMob.setAllMobileLocations(locArray);
		return allMob;
	}

}
