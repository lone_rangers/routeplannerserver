package com.tom.engine;

import javax.servlet.http.HttpServlet;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.tom.dto.RPSGenericResponse;
import com.tom.dto.RPSMobileLocation;
import com.tom.dto.RpsOrderList;
import com.tom.dto.RpsOrders;
import com.tom.service.RPSOrderService;

@EnableAutoConfiguration
@RestController
public class MainEngine extends HttpServlet {

	@Autowired
	private RPSOrderService rPSOrderService;

	//store the orders
	@RequestMapping(value="/storeorders",method=RequestMethod.POST,consumes="application/json")
	public RPSGenericResponse storeorders(@RequestBody(required=true) RpsOrderList rpsOrderList){
		System.out.println("From contoller The number of Orders are:"+rpsOrderList.getRpsOrders().length);
		return rPSOrderService.storeorders(rpsOrderList);
		
	}

	//get the current orders
	//store the orders
	@RequestMapping(value="/getorders",method=RequestMethod.GET)
	public RpsOrderList getOrders(){
		return rPSOrderService.getOrders("This Will have no use");
	}

	//default method
	@RequestMapping(value="/",method=RequestMethod.GET)
	public String cathAll(){
		return "Server is running";
	}
	
}
