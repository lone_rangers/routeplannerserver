package com.tom.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tom.dao.RPSOrderDao;
import com.tom.dto.RPSGenericResponse;
import com.tom.dto.RpsOrderList;

@Service("rPSOrderService")
public class RPSOrderServiceImpl implements RPSOrderService {
	
	@Autowired
	private RPSOrderDao rpsOrderDao;

	public RPSGenericResponse storeorders(RpsOrderList rpsOrderList) {
		return rpsOrderDao.storeorders(rpsOrderList);
	}

	public RpsOrderList getOrders(String someParameter) {
		return rpsOrderDao.getOrders(someParameter);
	}

}
