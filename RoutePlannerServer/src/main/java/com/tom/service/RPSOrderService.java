package com.tom.service;

import com.tom.dto.RPSGenericResponse;
import com.tom.dto.RpsOrderList;

public interface RPSOrderService {
	
	RPSGenericResponse storeorders(RpsOrderList rpsOrderList);
	
	RpsOrderList getOrders(String someParameter);

}
